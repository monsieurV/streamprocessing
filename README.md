# README #

### What is this repository for? ###

* This repository is a simple library that manage RSS/atom aggregation.
* It store RSS/atom feed in a mongoDB database.

### How do I get set up? ###

* You need a working golang environment.
* In order to get it worked, you will need the [mgo](https://godoc.org/labix.org/v2/mgo). MongoDB driver for golang.

### Getting started ###
* To add a new feed just call the AddFeed function with the feed url in parameter.
* To get all feed update just call the UpdateAllFeeds function (or UpdateAllFeedsAsync for the async version)
* You can now connect your app to the mongo database to get feeds and add a cron job to keep your feeds up to date.