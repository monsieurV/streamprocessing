package models

import (
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"log"
)

type Item struct {
	Model `bson:",inline"`
	// The id of the feed
	Feed_id bson.ObjectId
	// The sha1 of the Item
	Sha1 string `bson:"sha1"`
	// A string that uniquely identifies the item.
	Identifier string `bson:"identifier"`
	// The title of the item.
	Title string `bson:"title"`
	// The link to the complete content of the item.
	Link string `bson:"link"`
	// Indicates when the item was published.
	PublicationDate string `bson:"publicationDate"`
	// Copyright notice for content in the channel.
	Copyright string `bson:"copyright"`
	// The item synopsis.
	Description string `bson:"description"`
	// A short summary, abstract, or excerpt of the entry.
	Sumary string `bson:"sumary"`
	// The authors of the item.
	Authors []Person `bson:"authors"`
	// The contributors of the item.
	Contributors []Person `bson:"contributors"`
	// Includes the item in one or more categories.
	Categories []string `bson:"categories"` //tmp string (see atom categories: scheme and label)
	// URL of a page for comments relating to the item.
	Comments string `bson:"comments"`
	// Describes a media object that is attached to the item.
	Enclosure Enclosure `bson:"enclosure"` //tmp string (see rss and atom standard)
	// The source item if this item is copied from one feed into another feed
	//SourceItem Item `bson:"sourceItem"` // tmp
}

type Enclosure struct {
	// Where the enclosure is located.
	Url string `bson:"url"`
	// what its type is, a standard MIME type.
	Type string `bson:"type"`
	// How big it is in bytes.
	Length string `bson:"length"`
}

func (this *Item) isValid() bool {
	return true
}

/*check if item is identique :
1 : check first GUID
2 :	check link
3 : check title and description
4 : sha1*/
func (this *Item) Save(mongo *MongoDB) error {
	_, err := GetItemCollection(mongo).Upsert(
		bson.M{
			"identifier":  this.Identifier,
			"link":        this.Link,
			"title":       this.Title,
			"description": this.Description,
			"sha1":        this.Sha1,
		}, this) //tmp

	if err != nil {
		//revel.WARN.Printf("Unable to save item: %v error %v", this, err)
		log.Print("Unable to save item: ", this, " error ", err)
	}

	return err
}

func (this *Item) Delete(mongo *MongoDB) error {
	err := GetItemCollection(mongo).RemoveId(this.Id)

	if err != nil {
		//revel.WARN.Printf("Undable to delete item: %v error %v", this, err)
		log.Print("Unable to delete item: ", this, " error ", err)
	}

	return err
}

func GetItemCollection(mongo *MongoDB) *mgo.Collection {
	return mongo.Collection("items")
}

func GetItemsByFeedObjectId(mongo *MongoDB, Id bson.ObjectId) []*Item {
	items := []*Item{}
	err := GetItemCollection(mongo).Find(bson.M{"feed_id": Id}).All(&items)

	if err != nil {
		panic(err)
	}

	return items
}
