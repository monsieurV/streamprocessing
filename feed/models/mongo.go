package models

import (
	"code.google.com/p/goconf/conf"
	//"fmt"
	"labix.org/v2/mgo"
	"log"
	"sync"
)

type MongoDB struct {
	Session    *mgo.Session
	DB         *mgo.Database
	Config     *conf.ConfigFile
	ServerName string
}

//singleton instance
var mongo_instance *MongoDB
var mongo_once sync.Once

//Mongo is an accessor for the mongo singleton
func GetMongoSingleton() *MongoDB {
	mongo_once.Do(func() {
		mongo_instance = new(MongoDB)

		config, err := conf.ReadConfigFile("feed/config/conf.cfg")
		mongo_instance.ServerName, err = config.GetString("database", "name")

		if err != nil {
			mongo_instance.ServerName = "local" //default value
		}

		mongo_instance.Config, err = conf.ReadConfigFile("feed/config/mongo.cfg")

		if err != nil {
			log.Fatal("error: ", err)
		}
	})

	return mongo_instance
}

func GetNewMongoSession() *MongoDB { // tmp
	mgo_instance := new(MongoDB)

	config, err := conf.ReadConfigFile("feed/config/conf.cfg")
	mgo_instance.ServerName, err = config.GetString("database", "name")

	if err != nil {
		mgo_instance.ServerName = "local" //default value
	}

	mgo_instance.Config, err = conf.ReadConfigFile("feed/config/mongo.cfg")

	if err != nil {
		log.Fatal("error: ", err)
	}

	return mgo_instance
}

func (this *MongoDB) GetUri() (string, error) {
	host, err := this.Config.GetString(this.ServerName, "host")

	if err != nil {
		log.Print("error: ", err)
		return "", err
	}

	user, err := this.Config.GetString(this.ServerName, "user")
	if err != nil {
		return "mongodb://" + host, nil
	}

	password, err := this.Config.GetString(this.ServerName, "password")

	return "mongodb://" + user + ":" + password + "@" + host, nil
}

func (this *MongoDB) GetDatabaseName() string {
	databaseName, err := this.Config.GetString(this.ServerName, "database")

	if err != nil {
		log.Print("error: ", err)
		return ""
	}

	return databaseName
}

//Connect connects use to the mongo server
func (this *MongoDB) Connect() { //tmp check if allready connected
	uri, err := this.GetUri()

	if err != nil {
		log.Fatal("error : ", err)
	}

	this.Session, err = mgo.Dial(uri)

	if err != nil {
		log.Fatal("error : ", err)
	}

	this.SetDB(this.GetDatabaseName())
}

//Connect connects use to the mongo server
func (this *MongoDB) Disconnect() {
	this.Session.Close()
}

//SetDB is an accessor to allow you to set the target DB
func (this *MongoDB) SetDB(name string) {
	this.DB = this.Session.DB(name)
}

//Collection is an accessor to get back a pointer to the collection object
func (this *MongoDB) Collection(name string) *mgo.Collection {
	return this.DB.C(name)
}
