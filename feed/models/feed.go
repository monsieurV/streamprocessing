package models

import (
	//"fmt"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"log"
)

type Feed struct {
	FeedHeader
	Items []Item //`bson:"item"`
}

type FeedHeader struct { //tmp
	Model `bson:",inline"`
	// The standard oh the feed (ex: rss, atom, ect...)
	Standard string `bson:"standard"`
	// The version of the standard
	StandardVersion string `bson:"standardVersion"`
	//The unique identifier of the feed. (Warning: It can be empty because not all standard content feed identifier!!)
	Identifier string `bson:"identifier"`
	// Url of the Feed
	Url string `bson:"url"`
	// The name of the channel.
	Title string `bson:"title"`
	// The URL to the HTML website corresponding to the channel.
	Link string `bson:"link"`
	// Phrase or sentence describing the channel.
	Description string `bson:"description"`
	// The language the channel is written in.
	Language string `bson:"language"`
	// Copyright notice for content in the channel.
	Copyright string `bson:"copyright"`
	// Specify one or more categories that the channel belongs to.
	Categories []string `bson:"categories"` //tmp string (see atom categories: scheme and label)
	// The last time the content of the channel changed.
	LastUpdateDate string `bson:"lastUpdateDate"` // tmp string
	// The program used to generate the channel.
	Generator Generator `bson:"generator"`
	// Specifies a GIF, JPEG or PNG image that can be displayed with the channel.
	Image Image `bson:"image"`
	// Specifies the favicon of the channel.
	Icon string `bson:"icon"`
	// The authors of the feed
	Authors []Person `bson:"authors"`
	// Contributors of the feed
	Contributors []Person `bson:"contributors"`
	// Email address for person responsible for editorial content.
	ManagingEditor string `bson:"managingEditor"`
	// Email address for person responsible for technical issues relating to channel.
	WebMaster string `bson:"webMaster"`
	//A URL that points to the documentation for the format used in the RSS file.
	Docs string `bson:"docs"`
	//Allows processes to register with a cloud to be notified of updates to the channel, implementing a lightweight publish-subscribe protocol for RSS feeds.
	Cloud Cloud `bson:"cloud"`
	//ttl stands for time to live. It's a number of minutes that indicates how long a channel can be cached before refreshing from the source.
	Ttl int `bson:"ttl"`
	//The PICS rating for the channel.
	Rating string `bson:"rating"`
	//Specifies a text input box that can be displayed with the channel.
	TextInput TextInput `bson:"textInput"`
	//A hint for aggregators telling them which hours they can skip.
	SkipHours []int `bson:"skipHours"`
	//A hint for aggregators telling them which days they can skip.
	SkipDays []string `bson:"skipDays"`
}

type Person struct {
	// A human-readable name for the person.
	Name string `bson:"name"`
	// The email of the person.
	Email string `bson:"email"`
	// A home page for the person.
	Uri string `bson:"uri"`
}

type Generator struct {
	// The name of the generator.
	Name string `bson:"name"`
	// The uri of the generator.
	Uri string `bson:"uri"`
	// The version of the generator.
	Version string `bson:"version"`
}

type Image struct {
	// The URL of a GIF, JPEG or PNG image that represents the channel.
	Url string `bson:"url"`
	// Describes the image, it's used in the ALT attribute of the HTML <img> tag when the channel is rendered in HTML.
	Title string `bson:"title"`
	// The URL of the site, when the channel is rendered, the image is a link to the site.
	Link string `bson:"link"`
	// The width of the image in pixels (max: 144, default: 88).
	Width string `bson:"width"`
	// The height of the image in pixels (max: 400, default: 31).
	Height string `bson:"height"`
	// Text that is included in the TITLE attribute of the link formed around the image in the HTML rendering.
	Description string `bson:"description"`
}

type TextInput struct {
	// The label of the Submit button in the text input area.
	Title string `bson:"title"`
	// Explains the text input area.
	Description string `bson:"description"`
	// The name of the text object in the text input area.
	Name string `bson:"name"`
	// The URL of the CGI script that processes text input requests.
	Link string `bson:"link"`
}

type Cloud struct {
	// The domain name or ip adress of the cloud (required).
	Domain string `bson:"domain"`
	// The TCP port that the cloud is running (required).
	Port int `bson:"port"`
	//  The location of the responder (required).
	Path string `bson:"path"`
	// The name of the procedure to call to request notification (required).
	RegisterProcedure string `bson:"registerProcedure"`
	// xml-rpc, soap, or http-post (case sensitive), indicateing the protocol is to be used (required).
	Protocole string `bson:"protocol"`
}

func (this *Feed) isValid() bool {
	return true
}

func (this *Feed) Save(mongo *MongoDB) error { //check validity
	if !this.FeedHeader.Id.Valid() {
		this.FeedHeader.Id = bson.NewObjectId()
	}

	_, err := GetFeedCollection(mongo).Upsert(bson.M{"_id": this.FeedHeader.Id}, this.FeedHeader)
	if err != nil {
		//revel.WARN.Printf("Unable to save feed: %v error %v", this, err)
		log.Print("Undable to save feed: ", this.FeedHeader, " error ", err)
		return err
	}

	for _, item := range this.Items {
		item.Feed_id = this.FeedHeader.Id
		//item.Sha1 = //tmp
		err = item.Save(mongo) //itemCollection.Insert(item)
	}

	return err
}

func (this *Feed) Delete(mongo *MongoDB) error { //tmp to test
	err := GetFeedCollection(mongo).RemoveId(this.FeedHeader.Id)
	if err != nil {
		//revel.WARN.Printf("Undable to delete feed: %v error %v", this, err)
		log.Print("Undable to delete feed: ", this, " error ", err)
	}
	return err
}

func GetFeedCollection(mongo *MongoDB) *mgo.Collection {
	return mongo.Collection("feeds")
}

func GetAllFeedHeaders(mongo *MongoDB) []*FeedHeader { //tmp usefull limit ????
	feedHeaders := []*FeedHeader{}
	err := GetFeedCollection(mongo).Find(nil).All(&feedHeaders)

	if err != nil {
		panic(err)
	}

	return feedHeaders
}

func GetFeedHeaderById(mongo *MongoDB, id string) *FeedHeader { //tmp to test
	ObjectId := bson.ObjectIdHex(id)
	return GetFeedHeaderByObjectId(mongo, ObjectId)
}

func GetFeedHeaderByObjectId(mongo *MongoDB, Id bson.ObjectId) *FeedHeader { //tmp to test
	feedHeader := new(FeedHeader)
	err := GetFeedCollection(mongo).FindId(Id).One(feedHeader)

	if err != nil {
		panic(err)
	}

	return feedHeader
}
