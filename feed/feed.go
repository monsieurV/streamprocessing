package feed

import (
	"./models"
	"./parser"
	"sync"
)

func AddFeed(url string) {
	var feed models.Feed
	feed.FeedHeader.Url = url
	mongo := models.GetMongoSingleton() //tmp don't do that here
	mongo.Connect()                     //tmp
	defer mongo.Disconnect()
	feed.Save(mongo)
}

func UpdateAllFeeds() {
	mongo := models.GetMongoSingleton() //tmp don't do that here
	mongo.Connect()
	defer mongo.Disconnect()

	feedHeaders := models.GetAllFeedHeaders(mongo)

	for _, feedHeader := range feedHeaders {
		doUpdateFeed(feedHeader)
	}
}

func UpdateAllFeedsAsync() { //tmp name
	mongo := models.GetMongoSingleton() //tmp don't do that here
	mongo.Connect()
	defer mongo.Disconnect()

	feedHeaders := models.GetAllFeedHeaders(mongo)

	var wg sync.WaitGroup

	wg.Add(len(feedHeaders))

	for _, feedHeader := range feedHeaders {
		go UpdateFeedAsync(feedHeader, &wg)
	}

	wg.Wait()
}

func UpdateFeed(feedHeader *models.FeedHeader) {
	mongo := models.GetMongoSingleton() //tmp don't do that here
	mongo.Connect()
	defer mongo.Disconnect()
	doUpdateFeed(feedHeader)
}

func doUpdateFeed(feedHeader *models.FeedHeader) {
	mongo := models.GetMongoSingleton()         //tmp don't do that here
	feed, err := parser.GetFeed(feedHeader.Url) //tmp error handle
	if err != nil {
		return //tmp log
	}
	feed.FeedHeader.Id = feedHeader.Id
	feed.FeedHeader.Url = feedHeader.Url //tmp
	feed.Save(mongo)
}

func UpdateFeedAsync(feedHeader *models.FeedHeader, wg *sync.WaitGroup) {
	defer wg.Done()
	mongo := models.GetNewMongoSession() //tmp don't do that here
	mongo.Connect()
	defer mongo.Disconnect()
	feed, err := parser.GetFeed(feedHeader.Url) //tmp duplicate code
	if err != nil {
		return //tmp log
	}
	feed.FeedHeader.Id = feedHeader.Id
	feed.FeedHeader.Url = feedHeader.Url //tmp
	feed.Save(mongo)
}
