package parser

import (
	"../models"
	"./standard"
	//"./standard/atom_0_3"
	//"./standard/atom_1_0"
	//"./standard/rss_1_0"
	"./standard/rss_2_0"
	"io/ioutil"
	"net/http"
	//"log"
)

func GetFeedString(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return "", err
	}

	return string(body), nil
}

func GetFeed(url string) (models.Feed, error) {
	feedString, err := GetFeedString(url)

	if err != nil {
		return models.Feed{}, err
	}

	feedStandard := GetFeedStandard(feedString)

	feedStandard.Parse(feedString)

	return feedStandard.ToFeedModel(), nil
}

func GetFeedStandard(feedString string) standard.Feed { //tmp, to implemente
	feedStandard := new(rss_2_0.Feed)
	return feedStandard
}
