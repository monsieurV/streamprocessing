package standard

import (
	"../../models"
)

type Feed interface {
	Parse(feedString string) error
	ToFeedModel() models.Feed
	Display()
}
