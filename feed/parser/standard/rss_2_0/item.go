package rss_2_0

import (
	"../../../models"
	"encoding/xml"
)

type Item struct {
	XMLName xml.Name `xml:"item"`
	//The title of the item (one of title or description must be present).
	Title string `xml:"title" bson:"title"`
	//The URL of the item.
	Link string `xml:"link"`
	//The item synopsis (one of title or description must be present).
	Description string `xml:"description"`
	//Email address of the author of the item.
	Author string `xml:"author"`
	//Includes the item in one or more categories.
	Category []string `xml:"category"` //tmp attr domain
	//URL of a page for comments relating to the item.
	Comments string `xml:"comments"`
	//Describes a media object that is attached to the item.
	Enclosure Enclosure `xml:"enclosure"`
	//A string that uniquely identifies the item.
	Guid string `xml:"guid"`
	//Indicates when the item was published.
	PubDate string `xml:"pubDate"`
	//The RSS channel that the item came from.
	Source string `xml:"source"`
}

type Enclosure struct {
	XMLName xml.Name `xml:"enclosure"`
	// Where the enclosure is located (required).
	Url string `xml:"url,attr"`
	// what its type is, a standard MIME type (required).
	Type string `xml:"type,attr"`
	// How big it is in bytes (required).
	Length string `xml:"length,attr"`
}

func (this *Item) ToItemModel() models.Item {
	var item models.Item

	item.Sha1 = "" //tmp
	item.Identifier = this.Guid
	item.Title = this.Title
	item.Link = this.Link
	item.PublicationDate = this.PubDate //tmp format data !!
	//item.Copyright
	item.Description = this.Description
	//item.Sumary
	//item.Contributors
	item.Categories = this.Category
	item.Comments = this.Comments
	item.Enclosure.Url = this.Enclosure.Url
	item.Enclosure.Type = this.Enclosure.Type
	item.Enclosure.Length = this.Enclosure.Length

	if this.Author != "" {
		item.Authors = make([]models.Person, 1)
		item.Authors[0].Email = this.Author
	}

	//item.SourceItem //tmp ?

	return item
}
