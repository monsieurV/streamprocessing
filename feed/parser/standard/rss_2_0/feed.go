package rss_2_0

import (
	"../../../models"
	"../../standard"
	"encoding/xml"
	"fmt"
	"log"
)

type Feed struct {
	standard.Feed
	XMLName xml.Name `xml:"rss"`
	Channel Channel  `xml:"channel"`
}

type Channel struct {
	XMLName xml.Name `xml:"channel"`
	//Url string//tmp
	// The name of the channel (required).
	Title string `xml:"title"`
	// The URL to the HTML website corresponding to the channel (required).
	Link string `xml:"link"`
	// Phrase or sentence describing the channel (required).
	Description string `xml:"description"`
	// The language the channel is written in.
	Language string `xml:"language"`
	// Copyright notice for content in the channel.
	Copyright string `xml:"copyright"`
	// Email address for person responsible for editorial content.
	ManagingEditor string `xml:"managingEditor"`
	// Email address for person responsible for technical issues relating to channel.
	WebMaster string `xml:"webMaster"`
	// The publication date for the content in the channel.
	PubDate string `xml:"pubDate"`
	// The last time the content of the channel changed.
	LastBuildDate string `xml:"lastBuildDate"`
	// Specify one or more categories that the channel belongs to.
	Categories []string `xml:"category"` //tmp attr domain
	// A string indicating the program used to generate the channel.
	Generator string `xml:"generator"`
	// A URL that points to the documentation for the format used in the RSS file.
	Docs string `xml:"docs"`
	// Allows processes to register with a cloud to be notified of updates to the channel, implementing a lightweight publish-subscribe protocol for RSS feeds.
	Cloud Cloud `xml:"cloud"` //tmp attr
	// ttl stands for time to live. It's a number of minutes that indicates how long a channel can be cached before refreshing from the source.
	Ttl int `xml:"ttl"`
	// Specifies a GIF, JPEG or PNG image that can be displayed with the channel.
	Image Image `xml:"image"`
	// The PICS rating for the channel.
	Rating string `xml:"rating"`
	// Specifies a text input box that can be displayed with the channel.
	TextInput TextInput `xml:"textInput"`
	// A hint for aggregators telling them which hours they can skip.
	SkipHours SkipHours `xml:"skipHours"`
	// A hint for aggregators telling them which days they can skip.
	SkipDays SkipDays `xml:"skipDays"`
	// The items of the feed.
	Items []Item `xml:"item"`
}

type Image struct {
	XMLName xml.Name `xml:"image"`
	// The URL of a GIF, JPEG or PNG image that represents the channel (required).
	Url string `xml:"url"`
	// Describes the image, it's used in the ALT attribute of the HTML <img> tag when the channel is rendered in HTML (required).
	Title string `xml:"title"`
	// The URL of the site, when the channel is rendered, the image is a link to the site (required).
	Link string `xml:"link"`
	// The width of the image in pixels (max: 144, default: 88).
	Width string `xml:"width"`
	// The height of the image in pixels (max: 400, default: 31).
	Height string `xml:"height"`
	// Text that is included in the TITLE attribute of the link formed around the image in the HTML rendering.
	Description string `xml:"description"`
}

type TextInput struct {
	XMLName xml.Name `xml:"textInput"`
	// The label of the Submit button in the text input area (required).
	Title string `xml:"title"`
	// Explains the text input area (required).
	Description string `xml:"description"`
	// The name of the text object in the text input area (required).
	Name string `xml:"name"`
	// The URL of the CGI script that processes text input requests (required).
	Link string `xml:"link"`
}

type Cloud struct {
	XMLName xml.Name `xml:"cloud"`
	// The domain name or ip adress of the cloud (required).
	Domain string `xml:"domain,attr"`
	// The TCP port that the cloud is running (required).
	Port int `xml:"port,attr"`
	//  The location of the responder (required).
	Path string `xml:"path,attr"`
	// The name of the procedure to call to request notification (required).
	RegisterProcedure string `xml:"registerProcedure,attr"`
	// xml-rpc, soap, or http-post (case sensitive), indicateing the protocol is to be used (required).
	Protocole string `xml:"protocol,attr"`
}

type SkipHours struct {
	XMLName xml.Name `xml:"skipHours"`
	// One or more number between 0 and 23. Aggregators may not read the channel on hours listed in.
	Hours []int `xml:"hour"`
}

type SkipDays struct {
	XMLName xml.Name `xml:"skipDays"`
	// One or more day of the week (Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday). Aggregators may not read the channel during days listed in.
	Days []string `xml:"day"`
}

func (this *Feed) Parse(feedString string) error {
	err := xml.Unmarshal([]byte(feedString), this)

	if err != nil {
		log.Print(err)
		return err
	}

	return nil
}

func (this *Feed) ToFeedModel() models.Feed {
	var feed models.Feed

	feed.Standard = "rss"
	feed.StandardVersion = "2.0"
	//feed.Identifier
	//feed.Url = this.Channel.Url//tmp?
	feed.Title = this.Channel.Title
	feed.Link = this.Channel.Link
	feed.Description = this.Channel.Description
	feed.Language = this.Channel.Language //tmp format data !!
	feed.Copyright = this.Channel.Copyright
	feed.Categories = this.Channel.Categories
	feed.LastUpdateDate = this.Channel.LastBuildDate //tmp format data !!
	//feed.Generator.Name
	feed.Generator.Uri = this.Channel.Generator
	//feed.Generator.Version
	feed.Image.Url = this.Channel.Image.Url
	feed.Image.Title = this.Channel.Image.Title
	feed.Image.Link = this.Channel.Image.Link
	feed.Image.Width = this.Channel.Image.Width
	feed.Image.Height = this.Channel.Image.Height
	feed.Image.Description = this.Channel.Image.Description
	//feed.Icon
	//feed.Authors
	//feed.Contributors
	feed.ManagingEditor = this.Channel.ManagingEditor
	feed.WebMaster = this.Channel.WebMaster
	feed.Docs = this.Channel.Docs
	feed.Cloud.Domain = this.Channel.Cloud.Domain
	feed.Cloud.Path = this.Channel.Cloud.Path
	feed.Cloud.Port = this.Channel.Cloud.Port
	feed.Cloud.Protocole = this.Channel.Cloud.Protocole
	feed.Cloud.RegisterProcedure = this.Channel.Cloud.RegisterProcedure
	feed.Ttl = this.Channel.Ttl
	feed.Rating = this.Channel.Rating
	feed.TextInput.Title = this.Channel.TextInput.Title
	feed.TextInput.Description = this.Channel.TextInput.Description
	feed.TextInput.Name = this.Channel.TextInput.Name
	feed.TextInput.Link = this.Channel.TextInput.Link

	feed.SkipHours = make([]int, len(this.Channel.SkipHours.Hours))

	for i, hour := range this.Channel.SkipHours.Hours {
		feed.SkipHours[i] = hour
	}

	feed.SkipDays = make([]string, len(this.Channel.SkipDays.Days))

	for i, day := range this.Channel.SkipDays.Days {
		feed.SkipDays[i] = day
	}

	feed.Items = make([]models.Item, len(this.Channel.Items))

	for i, item := range this.Channel.Items {
		feed.Items[i] = item.ToItemModel()
	}

	return feed
}

func (this *Feed) Display() {
	fmt.Println(this.Channel.Items[0].Title)
}
