package atom_0_3

import (
	"../../../models"
	"../../standard"
	"encoding/xml"
	"fmt"
	"log"
)

type Feed struct { // tmp xml:lang, xml:base
	standard.Feed
	XMLName xml.Name `xml:"feed"`

	// Identifies the feed using a universally unique and permanent URI.
	Id string `xml:"id"`
	// The name of the channel (required).
	Title string `xml:"title"`
	// Indicates the last time the feed was modified in a significant way (UTC timezone) (required).
	Modified string `xml:"modified"` // tmp data format
	// Many links related to the feed (the alternate link is recommended).
	Links []Link `xml:"link"`
	// Names one or many authors of the feed (recommended).
	Authors []Person `xml:"author"` //tmp -> check standard!
	// The language of the feed
	Lang string `xml:"lang,attr"`
	// The Uri of the feed
	Base string `xml:"base,attr"`
	// Names one or more contributors to the feed
	Contributors []Person `xml:"contributor"`
	// A string indicating the program used to generate the channel.
	Generator Generator `xml:"generator"`
	// Specifies the favicon of the channel.
	Icon string `xml:"icon"`
	// Specifies a larger image that can be displayed with the channel.
	Logo string `xml:"logo"`
	// Copyright notice for content in the feed.
	Copyright string `xml:"copyright"`
	// Contains a human-readable description or subtitle for the feed.
	Tagline string `xml:"tagline"`
	// The items of the feed.

	//Info string //tmp check standard
	//Created string //tmp check standard
	Items []Item `xml:"entry"`
}

type Category struct {
	XMLName xml.Name `xml:"category"`
	// identifies the category (required)
	Term string `xml:"term,attr"`
	// identifies the categorization scheme via a URI.
	Scheme string `xml:"scheme,attr"`
	// provides a human-readable label for display
	Label string `xml:"label,attr"`
}

type Link struct { //tmp check standard
	XMLName xml.Name `xml:"link"`
	// The URI of the referenced resource (required)
	Href string `xml:"href,attr"`
	// A single link relationship type (default=alternate).
	Rel string `xml:"rel,attr"`
	// The media type of the resource.
	Type string `xml:"type,attr"`
	// A human readable information about the link, typically for display purposes.
	Title string `xml:"title,attr"`
}

type Generator struct {
	XMLName xml.Name `xml:"generator"`
	// The name of the generator.
	Name string `xml:",chardata"` //tmp check standard ??
	// The uri of the generator.
	Url string `xml:"url,attr"`
	// The version of the generator.
	Version string `xml:"version,attr"` //tmp check standard ??
}

type Person struct {
	// A human-readable name for the person.
	Name string `xml:"name"`
	// The email of the person.
	Email string `xml:"email"`
	// A home page for the person.
	Url string `xml:"url"`
}

func (this *Feed) Parse(feedString string) error {
	err := xml.Unmarshal([]byte(feedString), this)

	if err != nil {
		log.Print(err)
		return err
	}

	return nil
}

func (this *Feed) ToFeedModel() models.Feed {
	var feed models.Feed

	feed.Standard = "atom"
	feed.StandardVersion = "0.3"
	//feed.Identifier
	//feed.Url = this.Channel.Url//tmp
	feed.Title = this.Title
	feed.Description = this.Tagline
	feed.Language = this.Lang
	feed.Copyright = this.Copyright
	feed.LastUpdateDate = this.Modified
	feed.Generator.Name = this.Generator.Name //tmp ??
	feed.Generator.Uri = this.Generator.Url
	feed.Generator.Version = this.Generator.Version //tmp ??
	feed.Image.Url = this.Logo
	//feed.Image.Title = this.Channel.Image.Title
	//feed.Image.Link = this.Channel.Image.Link
	//feed.Image.Width = this.Channel.Image.Width
	//feed.Image.Height = this.Channel.Image.Height
	//feed.Image.Description = this.Channel.Image.Description
	feed.Icon = this.Icon
	//feed.ManagingEditor = this.Channel.ManagingEditor
	//feed.WebMaster = this.Channel.WebMaster
	//feed.Docs = this.Channel.Docs
	//feed.Cloud = this.Channel.Cloud
	//feed.Ttl = this.Channel.Ttl
	//feed.Rating = this.Channel.Rating
	//feed.TextInput.Title = this.Channel.TextInput.Title
	//feed.TextInput.Description = this.Channel.TextInput.Description
	//feed.TextInput.Name = this.Channel.TextInput.Name
	//feed.TextInput.Link = this.Channel.TextInput.Link
	//feed.SkipHours = this.Channel.SkipHours
	//feed.SkipDays = this.Channel.SkipDays

	for _, link := range this.Links {
		switch link.Rel {
		case "related": //tmp
		case "self": //tmp
		//	feed.Url = link.href
		case "alternate", "":
			feed.Link = link.Href
		}
	}

	feed.Authors = make([]models.Person, len(this.Authors))
	for i, author := range this.Authors {
		feed.Authors[i].Email = author.Email
		feed.Authors[i].Uri = author.Url
		feed.Authors[i].Name = author.Name
	}

	feed.Contributors = make([]models.Person, len(this.Contributors))
	for i, contributor := range this.Contributors {
		feed.Contributors[i].Email = contributor.Email
		feed.Contributors[i].Uri = contributor.Url
		feed.Contributors[i].Name = contributor.Name
	}

	feed.Items = make([]models.Item, len(this.Items))
	for i, item := range this.Items {
		feed.Items[i] = item.ToItemModel()
	}

	return feed
}

func (this *Feed) Display() {
	fmt.Println(this.Items[0].Title)
}
