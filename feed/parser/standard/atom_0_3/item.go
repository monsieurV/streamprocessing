package atom_0_3

import (
	"../../../models"
	"encoding/xml"
)

type Item struct { // tmp
	XMLName xml.Name `xml:"entry"`
	// A universally unique and permanent URI that uniquely identifies the item.
	Id string `xml:"id"`
	//The title of the item (required).
	Title string `xml:"title" bson:"title"`
	//Indicates the last time the item was modified in a significant way (UTC timezone) (required).
	Modified string `xml:"modified"`
	// Names one or many authors of the item (recommended).
	Authors []Person `xml:"author"` //tmp -> check standard!
	// Contains or links to the complete content of the entry (recommended).
	Content string `xml:"content"` //tmp check mode (=xml for exemple)
	//Many links related to the item (the alternate link is recommended).
	Links []Link `xml:"link"`
	// Conveys a short summary, abstract, or excerpt of the entry (recommended).
	Summary string `xml:"summary"`
	// Names one or more contributors to the item.
	Contributors []Person `xml:"contributor"`
	// 	Contains the time of the initial creation or first availability of the item (required).
	Issued string `xml:"issued"`

	//The RSS channel that the item came from.
	//Source Item `xml:"source"` //tmp
}

func (this *Item) ToItemModel() models.Item {
	var item models.Item

	item.Sha1 = "" //tmp
	item.Identifier = this.Id
	item.Title = this.Title
	item.PublicationDate = this.Issued
	item.Description = this.Content
	item.Sumary = this.Summary
	//item.Comments = this.Comments
	//item.Enclosure = this.Enclosure

	for _, link := range this.Links {
		switch link.Rel {
		case "related": //tmp
		case "enclosure": //tmp
			item.Enclosure.Url = link.Href
			item.Enclosure.Type = link.Type
		//case "via"://useless??
		case "alternate", "":
			item.Link = link.Href
		}
	}

	item.Authors = make([]models.Person, len(this.Authors))
	for i, author := range this.Authors {
		item.Authors[i].Email = author.Email
		item.Authors[i].Uri = author.Url
		item.Authors[i].Name = author.Name
	}

	item.Contributors = make([]models.Person, len(this.Contributors))
	for i, contributor := range this.Contributors {
		item.Contributors[i].Email = contributor.Email
		item.Contributors[i].Uri = contributor.Url
		item.Contributors[i].Name = contributor.Name
	}

	//item.SourceItem //tmp ?

	return item
}
