package atom_1_0

import (
	"../../../models"
	"../../standard"
	"encoding/xml"
	"fmt"
	"log"
)

type Feed struct { // tmp xml:lang, xml:base
	standard.Feed
	XMLName xml.Name `xml:"feed"`

	// Identifies the feed using a universally unique and permanent URI (required).
	Id string `xml:"id"`
	// The name of the channel (required).
	Title string `xml:"title"` //tmp type attr
	// Indicates the last time the feed was modified in a significant way (required).
	Updated string `xml:"updated"` // tmp data format
	// Many links related to the feed (the alternate link is recommended).
	Links []Link `xml:"link"`
	// Names one or many authors of the feed (recommended).
	Authors []Person `xml:"author"`
	// The language of the feed
	Lang string `xml:"lang,attr"`
	// The Uri of the feed
	Base string `xml:"base,attr"`
	// Specify one or more categories that the channel belongs to.
	Categories []Category `xml:"category"`
	// Names one or more contributors to the feed
	Contributors []Person `xml:"contributor"`
	// A string indicating the program used to generate the channel.
	Generator Generator `xml:"generator"`
	// Specifies the favicon of the channel.
	Icon string `xml:"icon"`
	// Specifies a larger image that can be displayed with the channel.
	Logo string `xml:"logo"`
	// Copyright notice for content in the feed.
	Right string `xml:"right"`
	// Contains a human-readable description or subtitle for the feed.
	Subtitle string `xml:"subtitle"` //tmp type attr
	// The items of the feed.
	Items []Item `xml:"entry"`
}

type Category struct {
	XMLName xml.Name `xml:"category"`
	// identifies the category (required)
	Term string `xml:"term,attr"`
	// identifies the categorization scheme via a URI.
	Scheme string `xml:"scheme,attr"`
	// provides a human-readable label for display
	Label string `xml:"label,attr"`
}

type Link struct {
	XMLName xml.Name `xml:"link"`
	// The URI of the referenced resource (required)
	Href string `xml:"href,attr"`
	// A single link relationship type (default=alternate).
	Rel string `xml:"rel,attr"`
	// The media type of the resource.
	Type string `xml:"type,attr"`
	// The language of the referenced resource.
	Hreflang string `xml:"hreflang,attr"`
	// A human readable information about the link, typically for display purposes.
	Title string `xml:"title,attr"`
	// The length of the resource, in bytes.
	Length string `xml:"length,attr"`
}

type Generator struct {
	XMLName xml.Name `xml:"generator"`
	// The name of the generator.
	Name string `xml:",chardata"`
	// The uri of the generator.
	Uri string `xml:"uri,attr"`
	// The version of the generator.
	Version string `xml:"version,attr"`
}

type Person struct {
	// A human-readable name for the person.
	Name string `xml:"name"`
	// The email of the person.
	Email string `xml:"email"`
	// A home page for the person.
	Uri string `xml:"uri"`
}

func (this *Feed) Parse(feedString string) error {
	err := xml.Unmarshal([]byte(feedString), this)

	if err != nil {
		log.Print(err)
		return err
	}

	return nil
}

func (this *Feed) ToFeedModel() models.Feed {
	var feed models.Feed

	feed.Standard = "atom"
	feed.StandardVersion = "1.0"
	//feed.Identifier
	//feed.Url = this.Channel.Url//tmp
	feed.Title = this.Title
	feed.Description = this.Subtitle
	feed.Language = this.Lang
	feed.Copyright = this.Right
	feed.LastUpdateDate = this.Updated
	feed.Generator.Name = this.Generator.Name
	feed.Generator.Uri = this.Generator.Uri
	feed.Generator.Version = this.Generator.Version
	feed.Image.Url = this.Logo
	//feed.Image.Title = this.Channel.Image.Title
	//feed.Image.Link = this.Channel.Image.Link
	//feed.Image.Width = this.Channel.Image.Width
	//feed.Image.Height = this.Channel.Image.Height
	//feed.Image.Description = this.Channel.Image.Description
	feed.Icon = this.Icon
	//feed.ManagingEditor = this.Channel.ManagingEditor
	//feed.WebMaster = this.Channel.WebMaster
	//feed.Docs = this.Channel.Docs
	//feed.Cloud = this.Channel.Cloud
	//feed.Ttl = this.Channel.Ttl
	//feed.Rating = this.Channel.Rating
	//feed.TextInput.Title = this.Channel.TextInput.Title
	//feed.TextInput.Description = this.Channel.TextInput.Description
	//feed.TextInput.Name = this.Channel.TextInput.Name
	//feed.TextInput.Link = this.Channel.TextInput.Link
	//feed.SkipHours = this.Channel.SkipHours
	//feed.SkipDays = this.Channel.SkipDays

	for _, link := range this.Links {
		switch link.Rel {
		case "related": //tmp
		case "self": //tmp
		//	feed.Url = link.href
		case "alternate", "":
			feed.Link = link.Href
		}
	}

	feed.Categories = make([]string, len(this.Categories))
	for i, category := range this.Categories {
		feed.Categories[i] = category.Term
		//category.Label
		//category.Scheme
	}

	feed.Authors = make([]models.Person, len(this.Authors))
	for i, author := range this.Authors {
		feed.Authors[i].Email = author.Email
		feed.Authors[i].Uri = author.Uri
		feed.Authors[i].Name = author.Name
	}

	feed.Contributors = make([]models.Person, len(this.Contributors))
	for i, contributor := range this.Contributors {
		feed.Contributors[i].Email = contributor.Email
		feed.Contributors[i].Uri = contributor.Uri
		feed.Contributors[i].Name = contributor.Name
	}

	feed.Items = make([]models.Item, len(this.Items))
	for i, item := range this.Items {
		feed.Items[i] = item.ToItemModel()
	}

	return feed
}

func (this *Feed) Display() {
	fmt.Println(this.Items[0].Title)
}
