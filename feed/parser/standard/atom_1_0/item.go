package atom_1_0

import (
	"../../../models"
	"encoding/xml"
)

type Item struct { // tmp
	XMLName xml.Name `xml:"entry"`
	// A universally unique and permanent URI that uniquely identifies the item (required).
	Id string `xml:"id"`
	//The title of the item (required).
	Title string `xml:"title" bson:"title"` //tmp type attr
	//Indicates the last time the item was modified in a significant way (required).
	Updates string `xml:"updated"`
	// Names one or many authors of the item (recommended).
	Authors []Person `xml:"author"`
	// Contains or links to the complete content of the entry (recommended).
	Content string `xml:"content"` //tmp type attr
	//Many links related to the item (the alternate link is recommended).
	Links []Link `xml:"link"`
	// Conveys a short summary, abstract, or excerpt of the entry (recommended).
	Summary string `xml:"summary"` //tmp type attr
	//Includes the item in one or more categories.
	Categories []Category `xml:"category"`
	// Names one or more contributors to the item.
	Contributors []Person `xml:"contributor"`
	// 	Contains the time of the initial creation or first availability of the item.
	Published string `xml:"published"`
	// Copyright notice for content in the item.
	Right string `xml:"right"`

	//The RSS channel that the item came from.
	//Source Item `xml:"source"` //tmp
}

func (this *Item) ToItemModel() models.Item {
	var item models.Item

	item.Sha1 = "" //tmp
	item.Identifier = this.Id
	item.Title = this.Title
	item.PublicationDate = this.Published
	item.Copyright = this.Right
	item.Description = this.Content
	item.Sumary = this.Summary
	//item.Comments = this.Comments
	//item.Enclosure = this.Enclosure

	for _, link := range this.Links {
		switch link.Rel {
		case "related": //tmp
		case "enclosure": //tmp
			item.Enclosure.Url = link.Href
			item.Enclosure.Type = link.Type
			item.Enclosure.Length = link.Length
		//case "via"://useless??
		case "alternate", "":
			item.Link = link.Href
		}
	}

	item.Categories = make([]string, len(this.Categories))
	for i, category := range this.Categories {
		item.Categories[i] = category.Term
		//category.Label
		//category.Scheme
	}

	item.Authors = make([]models.Person, len(this.Authors))
	for i, author := range this.Authors {
		item.Authors[i].Email = author.Email
		item.Authors[i].Uri = author.Uri
		item.Authors[i].Name = author.Name
	}

	item.Contributors = make([]models.Person, len(this.Contributors))
	for i, contributor := range this.Contributors {
		item.Contributors[i].Email = contributor.Email
		item.Contributors[i].Uri = contributor.Uri
		item.Contributors[i].Name = contributor.Name
	}

	//item.SourceItem //tmp ?

	return item
}
