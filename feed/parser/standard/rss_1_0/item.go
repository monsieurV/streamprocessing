package rss_1_0

import (
	"../../../models"
	"encoding/xml"
)

type Item struct { // tmp
	XMLName xml.Name `xml:"item"`
	//The title of the item (one of title or description must be present).
	Title string `xml:"title" bson:"title"`
	//The URL of the item.
	Link string `xml:"link"`
	//The item synopsis (one of title or description must be present).
	Description string `xml:"description"`
}

func (this *Item) ToItemModel() models.Item {
	var item models.Item

	item.Sha1 = "" //tmp
	//item.Identifier
	item.Title = this.Title
	item.Link = this.Link
	//item.PublicationDate
	//item.Copyright
	item.Description = this.Description
	//item.Sumary
	//item.Contributors
	//item.Categories
	//item.Comments
	//item.Enclosure
	//item.Authors

	//item.SourceItem //tmp ?

	return item
}
