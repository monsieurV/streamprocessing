package rss_1_0

import (
	"../../../models"
	"../../standard"
	"encoding/xml"
	"fmt"
	"log"
)

type Feed struct {
	standard.Feed
	XMLName xml.Name `xml:"RDF"`
	Channel Channel  `xml:"channel"`
	// Specifies a GIF, JPEG or PNG image that can be displayed with the channel (required if image field is present).
	Image Image `xml:"image"`
	// Specifies a text input box that can be displayed with the channel (required if textinput field is present).
	TextInput TextInput `xml:"textinput"`

	//the items of the feed
	Items []Item `xml:"item"`
}

type Channel struct { // tmp rdf:about
	XMLName xml.Name `xml:"channel"`
	//Url string // tmp
	// The name of the channel (required).
	Title string `xml:"title"`
	// The URL to the HTML website corresponding to the channel (required).
	Link string `xml:"link"`
	// Phrase or sentence describing the channel.
	Description string `xml:"description"`

	//usefull ???
	//Image `xml:"image"`//tmp (rdf:resource)
	//TextInput  `xml:"textinput"`//tmp
	//Items   []   `xml:"items"`//tmp
}

type Image struct {
	XMLName xml.Name `xml:"image"`
	// The URL of a GIF, JPEG or PNG image that represents the channel (required).
	Url string `xml:"url"`
	// Describes the image, it's used in the ALT attribute of the HTML <img> tag when the channel is rendered in HTML (required).
	Title string `xml:"title"`
	// The URL of the site, when the channel is rendered, the image is a link to the site (required).
	Link string `xml:"link"`
}

type TextInput struct { //tmp rdf:about
	XMLName xml.Name `xml:"textinput"`
	// The label of the Submit button in the text input area (required).
	Title string `xml:"title"`
	// Explains the text input area (required).
	Description string `xml:"description"`
	// The name of the text object in the text input area (required).
	Name string `xml:"name"`
	// The URL of the CGI script that processes text input requests (required).
	Link string `xml:"link"`
}

func (this *Feed) Parse(feedString string) error {
	err := xml.Unmarshal([]byte(feedString), this)

	if err != nil {
		log.Print(err)
		return err
	}

	return nil
}

func (this *Feed) ToFeedModel() models.Feed {
	var feed models.Feed

	feed.Standard = "rss"
	feed.StandardVersion = "1.0"
	//feed.Identifier
	//feed.Url //tmp
	feed.Title = this.Channel.Title
	feed.Link = this.Channel.Link
	feed.Description = this.Channel.Description
	//feed.Language  //tmp format data !!
	//feed.Copyright
	//feed.Categories
	//feed.LastUpdateDate
	//feed.Generator.Name
	//feed.Generator.Uri
	//feed.Generator.Version
	feed.Image.Url = this.Image.Url
	feed.Image.Title = this.Image.Title
	feed.Image.Link = this.Image.Link
	//eed.Image.Width
	//feed.Image.Height
	//feed.Image.Description
	//feed.Icon
	//feed.Authors
	//feed.Contributors
	//feed.ManagingEditor
	//feed.WebMaster
	//feed.Docs
	//eed.Cloud
	//feed.Ttl
	//feed.Rating
	feed.TextInput.Title = this.TextInput.Title
	feed.TextInput.Description = this.TextInput.Description
	feed.TextInput.Name = this.TextInput.Name
	feed.TextInput.Link = this.TextInput.Link
	//feed.SkipHours
	//feed.SkipDays

	feed.Items = make([]models.Item, len(this.Items))

	for i, item := range this.Items {
		feed.Items[i] = item.ToItemModel()
	}

	return feed
}

func (this *Feed) Display() {
	fmt.Println(this.Channel.Title)
}
